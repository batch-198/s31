/*
	What is a client?

	A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server.


	What is a server?

	A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.


	What is Node.js?

	Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with Javascript. Because by default, JS was conceptualized solely to the front end.


	Why is NodeJS popular?

	Performance - NodeJS is one of the most performing environment for creating backend applications with JS.

	Familiarity - Since NodeJS is built and uses JS as its language, it is very familiar for most developers.

	NPM - Node Package Manager is the largest registry for node packages. Packages are bits of program, methods, functions, codes that greatly help in the development of an application


*/

// console.log("Hello World!");

let http = require("http");

// console.log(http);
// require() is a built in JS method which allows us to import packages. Packages are pieces of code we can integrate to our application

// "http" is a default package that comes with NodeJS. It allows us to user methods that let us create servers

// http is a modile. Modules are packages we imported.
// Modules are objects that contains codes, methods, or data


// The http module let us create a server which is able to communicate with client through the use of Hypertext Transfer Protocol


// protocol to client-server communication - http://localhost:4000 - server/application

http.createServer(function(request,response){

	/*
		createServer() method is a method from the http module that allows us to handle requests and responses from a client and a server respectively,

		.createServer() method takes a function argument which is able to receive 2 objects. The request object which contains the details of the request from the client. The response object which contains the details of the response from the server.

		.createServer() always receives the request object first before the response

		response.writeHead() - is a method of the reponse object. It allows us to add header to our response. Headers are additional information about our response.

		We have 2 arguments in the writeHead() method.
		First is the HTTP status code. It is just a numerical code to let the client know about the status of their request:

		200 - request means OK
		401 - Unauthorized
		403 - Request forbidden
		404 - Resource cannot be found 

		reponse.end() it ends our response. It is also able to send message/data as a string.
	*/

	/*

		Servers can respond differently with different requests.

		We start our request with our URL.

		https://localhost:4000/ - A client can start a different request with a different URL.

		http://localhost:4000/ is not the same http://localhost:4000/profile 
		
		/ - URL endppoint (default)
		/profile - URL endpoint

		We can differentiate requests by their endpoints, we should be able to respond differently to different endpoints

		Information about the URL endpoint of the request is in the request object.
		
		request.url contains the URL endpoint

		/ - default endpoint - request URL: http://localhost:4000/

		/favicon.ico - browser's default behavior to retrieve the favicon

		/profile - request URL - http://localhost:4000/profile

		different request require different response

		The process or way to respond differently to a request is called a route.

		URLs ARE STRING

	*/

	// console.log(request.url);

	// specific response to a specific endpoint
	if(request.url === "/"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		// response.writeHead(StatusCode,{'Data Type'});
		response.end("Hello from our first server! This is from / endpoint.");

	} else if(request.url === "/profile"){
		
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Hi i'm Mika!");
	
	}



}).listen(4000);

	/*

		.listen() allows us to assign a port to our server. This will allow us serve our index.js server in our loval machine assigned to port 4000. There are several tasks and processes on our computer/machine that run on different port numbers.

		
		http://localhost:4000/
		 > http - hypertext transfer protocol
		 > localhost - locol machine/computer
		 > 4000 - current port assigned to our server

		 4000, 4040, 8000, 5000, 3000, 4200 - ports usually used for web development
		

		console.log() is added to show validation which port number our server is running on. In fact, the more complex our servers become, the more things we can check first before running the server.

	*/

console.log("Server is running on localHost:4000!");