let http = require("http");

http.createServer(function(request,response){

	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello from our sample Server!");

}).listen(5000);

console.log("Server is running on localHost:5000!");